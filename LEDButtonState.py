import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

LED = 12
GPIO.setup(LED, GPIO.OUT)
BUTTON = 16
GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)

led_state = 0
# States:
# 0 - Initial, Off
# 1 - On, Steady
# 2 - Off, Steady
# 3 - Flash
# 4 - Flicker Slow
# 5 - Flicker Fast
# 6 - Flicker Long
# 7 - Flicker Two-Phase
# 8 - Flicker Flash

led_max_state = 8
button_debounce = True
new_state = False
phase_count = 0

try:
	while True:
		# Check the button and rotate the state wheel
		button_state = GPIO.input(BUTTON)
		if button_state == False:
			if button_debounce:
				button_debounce = False
				if led_state == led_max_state:
					led_state = 0
				else:
					led_state = led_state + 1
				new_state = True
				print("Button pressed: New State: %s" % (led_state))
			time.sleep(0.1)# Give the button a chance to bounce
		else:
			button_debounce = True

		# Make the LED do its magic - based on state
		if led_state == 0:
			if new_state:
				GPIO.output(LED, GPIO.LOW)
				new_state = False
		elif led_state == 1:
			if new_state:
				GPIO.output(LED, GPIO.HIGH)
				new_state = False
		elif led_state == 2:
			if new_state:
				GPIO.output(LED, GPIO.LOW)
				new_state = False
		elif led_state == 3:
			GPIO.output(LED, GPIO.HIGH)
			time.sleep(0.5)
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.5)
			# As long as we are still in this state, we'll be back here in nanoseconds
		elif led_state == 4:
			GPIO.output(LED, GPIO.HIGH)
			time.sleep(0.05)
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.05)
		elif led_state == 5:
			GPIO.output(LED, GPIO.HIGH)
			time.sleep(0.005)
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.04)
		elif led_state == 6:
			GPIO.output(LED, GPIO.HIGH)
			time.sleep(0.05)
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.1)
		elif led_state == 7:
			if phase_count < 11:
				phase_count = phase_count + 1
				GPIO.output(LED, GPIO.HIGH)
				time.sleep(0.005)
				GPIO.output(LED, GPIO.LOW)
				time.sleep(0.04)
			elif phase_count < 18:
				phase_count = phase_count + 1
				GPIO.output(LED, GPIO.HIGH)
				time.sleep(0.05)
				GPIO.output(LED, GPIO.LOW)
				time.sleep(0.05)
			else:
				phase_count = 0 				
		elif led_state == 8:
			if phase_count < 11:
				phase_count = phase_count + 1
				GPIO.output(LED, GPIO.LOW)
				time.sleep(0.045)
			elif phase_count < 18:
				phase_count = phase_count + 1
				GPIO.output(LED, GPIO.HIGH)
				time.sleep(0.05)
				GPIO.output(LED, GPIO.LOW)
				time.sleep(0.05)
			else:
				phase_count = 0
		else:
			if new_state:
				print("State %s not implemented yet." % (led_state))
				new_state = False

except:
	GPIO.output(LED, GPIO.LOW)
	GPIO.cleanup()
	print("Off - Done")
