import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(12,GPIO.OUT)
for i in range(1,100):
	GPIO.output(12,GPIO.HIGH)
	time.sleep(0.1)
	GPIO.output(12,GPIO.LOW)
	time.sleep(0.1)
print "LED off"
