import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(12,GPIO.OUT)
GPIO.setup(16,GPIO.IN, pull_up_down=GPIO.PUD_UP)

led_state = False

try:
	while True:
		button_state = GPIO.input(16)
		if button_state == False:
			if led_state == False:
				print('Button Pressed...')
				led_state = True
				GPIO.output(12, GPIO.HIGH)
			time.sleep(0.1)
		else:
			led_state = False
			GPIO.output(12, GPIO.LOW)
except:
	GPIO.output(12, GPIO.LOW)
	GPIO.cleanup()
	print("Off - Done")
