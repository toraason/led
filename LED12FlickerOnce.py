import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

LED = 12
GPIO.setup(LED, GPIO.OUT)

phase_count = 0

try:
	while phase_count < 18:# Off
		if phase_count < 7:# Flicker
			phase_count = phase_count + 1
			GPIO.output(LED, GPIO.HIGH)
			time.sleep(0.05)
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.05)
		elif phase_count < 18:# Off
			phase_count = phase_count + 1
			GPIO.output(LED, GPIO.LOW)
			time.sleep(0.045)

except:
	GPIO.output(LED, GPIO.LOW)
	GPIO.cleanup()
	print("Off - Done")
