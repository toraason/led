# README #

### LED On Off on Pi ###

* Simple example of how to turn an LED on and off using GPIO
* Follow tutorial on: [The Pi Hut](https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins)
* **WARNING: Wiring and Resistors are required!**

* **WARNING: LED.py uses GPIO pin 18 - if you need to use GPIO 12 use LED12.py !**

### How do I get set up? ###

* Clone it `$ git clone {this-repo}` and `$ cd led` to it
* Run it `$ python LED.py` OR `$ python LED12.py`
* If does not work, try running python in sudo

### Who do I talk to? ###

* Owner: Dan Toraason `email: dan@toraason.com`
* I got the code from the Pi Hut example